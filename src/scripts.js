'use strict';

$ (function(){ 

  $('.header__farther').click(function() {
  	$(".header__caption div:not(.active)")
  		.addClass("active")
  		.siblings()
  		.removeClass("active")
  		.closest("#header__calc")
  		.find("div.header__tab")
  		.removeClass("active")
  		.eq($(this).index())
      .addClass("active");
    $('.header__num').text('2');
  });
  $('ul.technology__tabs-caption').on('click', 'li:not(.active)', function() {
    $(this)
      .addClass('active').siblings().removeClass('active')
      .closest('div.technology__tabs').find('div.technology__tabs-content').removeClass('active').eq($(this).index()).addClass('active');
  });

});