var gulp           = require('gulp'),
    browserSync    = require('browser-sync').create(),
    uglify         = require('gulp-uglifyjs'),
    less			= require('gulp-less'),
    autoprefixer   = require('gulp-autoprefixer'),
    cssnano			= require('gulp-cssnano'),
    imagemin       = require('gulp-imagemin'),
    rename			= require('gulp-rename'),
    rigger         = require('gulp-file-include'),
    sourcemaps     = require('gulp-sourcemaps'),
    watch          = require('gulp-watch'),
    concat         = require('gulp-concat'),
    uglifyjs		= require('gulp-uglifyjs');


var theme = './';

var path = {
    src: {
        root: "src",
        html: theme + 'src/**/*.html',
        less: theme + 'src/less',
        js: theme + 'src/js',
        libs: theme + 'src/libs',
    },
    dest: {
        html: theme + 'dist',
        css: theme + 'dist/css',
        js: theme + 'dist/js',
    },
    clean: './dist'
};

var config = {
    server: {
        baseDir: "./dist"
    },
    tunnel: false,
    host: 'localhost',
    port: 9000,
    logPrefix: "hs"
};

//html build
gulp.task('build:html', function () {
    gulp.src(path.src.root + '/*.html')
        .pipe(rigger())
        .pipe(gulp.dest(path.dest.html));
});

/* Собираем все JS библиотеки в один файл */
gulp.task('build:libsjs', function () {
    return gulp.src([
        'src/libs/jquery/jquery-3.3.1.min.js',
    ])
        .pipe(concat('libs.min.js'))
        .pipe(uglifyjs())
        .pipe(gulp.dest(path.dest.js));
});

/* Собираем все CSS библиотеки в один файл */
gulp.task('build:libscss', function () {
    return gulp.src([
        // 'src/path/to/library.min.css',
    ])
        .pipe(concat('libs.min.css'))
        .pipe(cssnano({zindex: false}))
        .pipe(gulp.dest(path.dest.css))
});

/* Компилируем style.less в css с сжатием */
gulp.task('build:less', function(){
    return gulp.src([
        'src/style.less',
        'src/sections/*/*.less'
    ])
        .pipe(less())
        .pipe(autoprefixer(['last 15 versions', '>1%', 'ie 8']))
        .pipe(cssnano({zindex: false}))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(path.dest.css))
        .pipe(browserSync.stream());
});

/* Собираем все js файлы в один */
gulp.task('build:scripts', function(){
    return gulp.src([
        'src/scripts.js',
        'src/sections/*/*.js'
    ])
        .pipe(concat('scripts.min.js'))
        .pipe(uglifyjs())
        .pipe(gulp.dest(path.dest.js))
});

gulp.task('watch', function(){

    browserSync.init({
        server: "./dist/"
    });

    gulp.watch(['src/sections/*/*.less', 'src/style.less'], ['build:less']);
    gulp.watch(['src/sections/*/*.js', 'src/scripts.js' ], ['build:scripts']);
    gulp.watch(['src/sections/*/*.html', 'src/*.html'], ['build:html']);

    browserSync.watch("src/**/*.*").on('change',browserSync.reload);
});


gulp.task('dev', ['build:html', 'build:libscss', 'build:libsjs','build:scripts','build:less']);